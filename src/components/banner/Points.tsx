import {
    StarIcon
  } from '@heroicons/react/outline'

  type Props = {
    points: number,
  }
  
  export const Points = ({ points } : Props) =>{
  
    return (
        <>
            <h1 className="grow text-2xl ml-2.5 justify-center text-center font-bold dark:text-white">
                {points}
            </h1>
            <StarIcon className='h-7 w-7 dark:stroke-white' />
        </>
    )
  }
  