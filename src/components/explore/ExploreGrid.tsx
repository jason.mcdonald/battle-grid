import { Piece, Location } from "../../lib/pieces"
import { ExploreRow } from "./ExploreRow"
import { Alert } from "../alerts/Alert"
import { useState } from "react"

type Props = {
  width: number,
  height: number,
  piece: Piece,
  placed: Location[],
  deadColumn: number,
  events: Location[],
  place: (location: Location) => void,
  hover: (location: Location) => void,
}

const extractRowPieces = (row: number, locations: Location[]) => {
  let rowPieces: Location[] = []

  for (let location of locations) {
	if (location.y === row) {
	  rowPieces.push(location)
	}
  }

  return rowPieces
}

const isValidPlacement = (width: number, height: number, selectedPiece: Piece, placed: Location[]) => {
	for (let location of placed) {
		for (let pointB of selectedPiece.dimensions) {
			let coordinate : Location = { x: pointB.x + selectedPiece.origin.x, y: pointB.y + selectedPiece.origin.y, color: 'None' }

			if ((location.y === coordinate.y && location.x === coordinate.x) || 
				(coordinate.x < 0 || coordinate.x >= width || coordinate.y < 0 || coordinate.y >= height)) {
				return false
			}
		}
	}

	for (let point of selectedPiece.dimensions) {
		if (point.x + selectedPiece.origin.x === 0 && placed.length === 0) {
			return true
		}
	}

	let directions = [
		{ x: -1, y: 0},
		{ x: 0, y: 1},
		{ x: 1, y: 0},
		{ x: 0, y: -1}
	]

	for (let location of placed) {
		for (let point of selectedPiece.dimensions) {
			for (let direction of directions) {
				let coordinate : Location = { 
					x: point.x + selectedPiece.origin.x + direction.x, 
					y: point.y + selectedPiece.origin.y + direction.y, 
					color: 'None' 
				}

				if (coordinate.x === location.x && coordinate.y === location.y) {
					return true
				}
			}
		}
	}

  return false 
}

export const ExploreGrid = ({ width, height, piece, placed, deadColumn, events, place, hover }: Props) => {

  const [alertMessage, setAlertMessage] = useState("")
  const [showAlert, setShowAlert] = useState(false)

  const handleCellHover = (row: number, column: number) => {
	hover({ x: column, y: row, color: piece.color })
  }

  const handleCellClick = (row: number, column: number) => {
	if (isValidPlacement(width, height, piece, placed)) {
	  place({ x: column, y: row, color: piece.color })
	} else {
	  setAlertMessage("Illegal Block Placement")
	  setShowAlert(true)
	  setTimeout(() => {
		setAlertMessage("")
		setShowAlert(false)
	  }, 500);
	}
  }

  return (
	<>
	  <div className="py-2">
		{Array.from(Array(height)).map((_, i) => (
		  <ExploreRow 
			width={width}
			key={i} 
			row={i} 
			selectedPiece={piece} 
			deadColumn={deadColumn}
			events={events}
			placed={extractRowPieces(i, placed)}
			hover={handleCellHover} 
			onClick={handleCellClick} />
		))}
	  </div>
	  <Alert isOpen={showAlert} message={alertMessage} />
	</>
  )
}
