import { Piece, Location, DEFAULT_PIECE } from "../../lib/pieces"
import { ExploreGrid } from "./ExploreGrid"
import { useState } from "react"

type Props = {
  piece: Piece,
  place: (location: Location) => void,
  hover: (location: Location) => void,
}

export const Explore = ({ piece, place, hover }: Props) => {

	const [deadColumn, setDeadColumn] = useState(-1)
	const [lastPiece, setLastPiece] = useState<Piece>(DEFAULT_PIECE)
	const [placed, setPlaced] = useState<Location[]>([])
	const [events, setEvents] = useState<Location[]>([
		{ x: 10, y: 2, color: 'purple'},
		{ x: 15, y: 9, color: 'purple'},
		{ x: 2, y: 3, color: 'Purple'},
		{ x: 8, y: 7, color: 'purple'},
		{ x: 6, y: 4, color: 'purple'},
		{ x: 16, y: 2, color: 'purple'},
		{ x: 12, y: 8, color: 'purple'},
	])

	const handlePlace = (location: Location) => {
		setPlaced([...placed, ...getLocations(piece)])
		setLastPiece(piece)
		place(location)

		console.log(lastPiece, location)
		let enemySpeed = placed.length < 25 ? 4 : 3

		setDeadColumn((placed.length - (placed.length % enemySpeed)) / enemySpeed)
	}

	const getLocations = (piece: Piece) => {
		return piece.dimensions.map((point) => { return {x: point.x + piece.origin.x, y: point.y + piece.origin.y, color: piece.color} })
	}

	return (
		<>
			<ExploreGrid 
				width={18}
				height={10}
				piece={piece} 
				placed={placed} 
				deadColumn={deadColumn} 
				events={events} 
				place={handlePlace} 
				hover={hover} />
		</>
	)
}
