import { Piece, Location } from "../../lib/pieces"
import { Cell, CellStatus } from "../grid/Cell"

type Props = {
  width: number,
  row: number,
  selectedPiece: Piece,
  placed: Location[],
  deadColumn: number,
  events: Location[],
  hover: (row: number, column: number) => void,
  onClick: (row: number, column: number) => void,
}

const extractCellStatus = (
    row: number, 
    column: number, 
    selectedPiece: Piece, 
    deadColumn: number,
    events: Location[],
    placed: Location[]): CellStatus => {

    let hoverStatus = getHoverStatus(row, column, deadColumn, events)
    
    for (let point of selectedPiece.dimensions) {
      let coordinate : Location = { x: point.x + selectedPiece.origin.x, y: point.y + selectedPiece.origin.y, color: 'None' }

      for (let location of placed) {
        if (coordinate.y === row && coordinate.x === column && coordinate.x === location.x && coordinate.y === location.y) {
          return { status: 'Hover', color: 'red', hoverStatus}
        }
      }

      if (coordinate.y === row && coordinate.x === column) {
        return { status: 'Hover', color: selectedPiece.color, hoverStatus}
      }
  }

    for (let location of placed) {
      if (location.y === row && location.x === column) {
          return { status: 'Owned', color: location.color, hoverStatus }
      }
    }

    return { status: 'None', color: 'None', hoverStatus }
}

const getHoverStatus = (row: number, column: number, deadColumn: number, events: Location[]) => {
  if (column <= deadColumn) return 'dying'

  return containsEvent(row, column, events) ? 'event' : 'none'
}

const containsEvent = (row: number, column: number,  events: Location[]) => {
    for (let event of events) {
        if (event.x === column && event.y === row) {
            return true
        }
    }

    return false
}

export const ExploreRow = ({ width, row, selectedPiece, placed, deadColumn, events, hover, onClick }: Props ) => {
  const emptyCells = Array.from(Array(width))

  return (
    <div className="flex justify-center mb-1">
      {emptyCells.map((_, i) => (
        <Cell 
            key={i} 
            column={i}
            status={extractCellStatus(row, i, selectedPiece, deadColumn, events, placed)}
            hover={(column: number) => hover(row, column)} 
            onClick={(column: number) => onClick(row, column)} 
        />
      ))}
    </div>
  )
}
