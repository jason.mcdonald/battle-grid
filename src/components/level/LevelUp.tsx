import { Piece, Location } from "../../lib/pieces"
import { Grid } from '../grid/Grid'
import { useState } from "react"
import { GRID_SIZE } from "../../constants/settings"

type Props = {
  piece: Piece,
  place: (location: Location) => void,
  hover: (location: Location) => void,
}

const WIDTH : number = GRID_SIZE
const HEIGHT : number = GRID_SIZE + 2

const isValidPlacement = (selectedPiece: Piece, placed: Location[]) => {
	for (let point of selectedPiece.dimensions) {
	  let coordinate : Location = { x: point.x + selectedPiece.origin.x, y: point.y + selectedPiece.origin.y, color: 'None' }
  
	  if (coordinate.y === HEIGHT - 1) {
		  return true
	  }
  
  
	  for (let location of placed) {
		  let coordinate : Location = { x: point.x + selectedPiece.origin.x, y: point.y + selectedPiece.origin.y, color: 'None' }
  
		  if (coordinate.x === location.x && coordinate.y === location.y - 1) {
			  return true
		  }
	  }
	}
  
	return false 
}

export const LevelUp = ({ piece, place, hover }: Props) => {

	const [experience, setExperience] = useState(10)
	const [level, setLevel] = useState(1)
	const [attack, setAttack] = useState(1)
	const [placed, setPlaced] = useState<Location[]>([])

	const handlePlace = (location: Location) => {
		if (experience <= 0 || !isValidPlacement(piece, placed)) return

		setAttack(extractAttack([...placed, ...getLocations(piece)]))
		setPlaced(processPieceAndLevelUp([...getLocations(piece)]))

		console.log({ level, attack: extractAttack([...placed, location]), experience})
		setExperience((prevExp) => prevExp - 1)
		place(location)
	}

	const getLocations = (piece: Piece) => {
		return piece.dimensions.map((point) => { return {x: point.x + piece.origin.x, y: point.y + piece.origin.y, color: piece.color} })
	}

	const extractAttack = (locations: Location[]) => {
		let highest = GRID_SIZE + 2

		for (let location of locations) {
			highest = Math.min(highest, location.y)
		}

		return 1 + (9 - highest)
	}

	const processPieceAndLevelUp = (pieces: Location[]) => {
		let newLevel = level
		let locations: Location[] = [...placed, ...pieces]

		while (findIndexOfLevelUp(locations) >= 0) {
			let levelUpIndex = findIndexOfLevelUp(locations)
			let newLocations: Location[] = []

			for (let location of locations) {
				if (location.y > levelUpIndex) {
					newLocations.push(location)
				} else if (location.y < levelUpIndex) {
					newLocations.push({...location, y: location.y + 1})
				}
			}

			newLevel++
			locations = newLocations
		}

		setLevel(newLevel)
		return locations
	}

	const findIndexOfLevelUp = (locations: Location[]) => {
		for (let i = 0; i < GRID_SIZE + 2; i++) {
			let count = 0
			for (let location of locations) {
				if (location.y === i) {
					count++
				}
			}

			if (count >= 8) {
				return i
			}
		}

		return -1
	}

	return (
		<>
			<Grid 
				width={WIDTH}
				height={HEIGHT} 
				piece={piece} 
				dead={[]} 
				owned={placed} 
				nextDeadCell={{ x: -1, y: -1, color: ''}} 
				place={handlePlace} 
				hover={hover} />
		</>
	)
}
