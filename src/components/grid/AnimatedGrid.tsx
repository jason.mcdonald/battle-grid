import { useState, useEffect } from 'react'
import { AnimatedRow } from './AnimatedRow'

type Props = {
    width: number,
    height: number,
    colorGrid: string[][]
}

export const AnimatedGrid = ({ width, height, colorGrid }: Props) => {

    const [animateIndex, setAnimateIndex] = useState(-20)

	useEffect(() => {
		if (animateIndex > 20) return

		const interval = setInterval(() => {
			setAnimateIndex((prevIndex) => prevIndex + 1)
		}, 50);
		return () => clearInterval(interval);
	}, []);

	return (
		<>
			{Array.from(Array(height)).map((_, i) => (
				<AnimatedRow key={i} width={width} colors={colorGrid[i]} animate={animateIndex < i} />
			))}
		</>
	)
}
