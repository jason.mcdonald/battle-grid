import classnames from 'classnames'
import { useState, useEffect } from 'react'

export type AnimationStatus = {
  color: string,
  animate: boolean
}

type Props = {
  id?: number,
  status?: AnimationStatus
}

const COLORS = [
    'blue',
    'orange',
    'green',
    'yellow',
    'purple'
]

export const AnimatedCell = ({ id, status }: Props) => {

	const [color, setColor] = useState(status?.animate ? 'blue' : status?.color)

	useEffect(() => {
		if (!status?.animate) return 
		
		const interval = setInterval(() => {
			setColor(COLORS[Math.floor(Math.random() * COLORS.length)])
		}, 50 + Math.floor(Math.random() * 85));
		return () => clearInterval(interval);
	}, []);

	if (color !== status?.color && !status?.animate) {
		setColor(status?.color)
	}

  const classes = classnames(
    'w-3 h-3 border-solid border-2 border-slate-500 flex items-center justify-center mx-0.5 font-bold rounded dark:text-white', {
      'cell-animation': !!id,
      'bg-sky-300': color === 'blue',
	  'bg-lime-300': color === 'green',
	  'bg-orange-300': color === 'orange',
	  'bg-yellow-300': color === 'yellow',
	  'bg-violet-300': color === 'purple',
	  'border-slate-800': color === 'none'
    }
  )

  return (
    <div className={classes}>

    </div>
  )
}
