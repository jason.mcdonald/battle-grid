import { AnimatedCell, AnimationStatus } from './AnimatedCell'

type Props = {
  width: number,
  colors: string[],
  animate: boolean
}

const extractCellStatus = (
    column: number, 
    colors: string[],
    animate: boolean): AnimationStatus => {
    
    return { color: colors[column], animate }
}

export const AnimatedRow = ({ width, colors, animate}: Props ) => {
  const emptyCells = Array.from(Array(width))

  return (
    <div className="flex justify-center mb-0.5">
      {emptyCells.map((_, i) => (
        <AnimatedCell 
            key={i} 
            status={extractCellStatus(i, colors, animate)}
        />
      ))}
    </div>
  )
}
