import { Piece, Location } from "../../lib/pieces"
import { CellStatus } from "./Cell"
import { Cell } from './Cell'

type Props = {
  width: number,
  row: number,
  selectedPiece: Piece,
  dead: Location[],
  owned: Location[],
  nextDeadCell: Location,
  hover: (row: number, column: number) => void,
  onClick: (row: number, column: number) => void,
}

const extractCellStatus = (
    row: number, 
    column: number, 
    selectedPiece: Piece, 
    owned: Location[], 
    deadCells: Location[], 
    nextDeadCell: Location): CellStatus => {
    
    let hoverStatus = getHoverStatus(row, column, nextDeadCell)

    for (let location of deadCells) {
        if (location.y === row && location.x === column) {
            return { status: 'Dead', color: 'slate', hoverStatus }
        }
    }

    for (let point of selectedPiece.dimensions) {
      let coordinate : Location = { x: point.x + selectedPiece.origin.x, y: point.y + selectedPiece.origin.y, color: 'None' }

      for (let location of owned) {
        if (coordinate.y === row && coordinate.x === column && coordinate.x === location.x && coordinate.y === location.y) {
          return { status: 'Hover', color: 'red', hoverStatus}
        }
      }

      if (coordinate.y === row && coordinate.x === column) {
        return { status: 'Hover', color: selectedPiece.color, hoverStatus}
      }
  }

    for (let location of owned) {
      if (location.y === row && location.x === column) {
          return { status: 'Owned', color: location.color, hoverStatus }
      }
    }

    return { status: 'None', color: 'None', hoverStatus }
}

const getHoverStatus = (row: number, column: number, nextDeadCell: Location) => {
  return isDying(row, column, nextDeadCell) ? 'dying' : 'None'
}

const isDying = (row: number, column: number, nextDeadCell: Location) => {

    return nextDeadCell && nextDeadCell.y === row && nextDeadCell.x === column
}

export const Row = ({ width, row, selectedPiece, owned, dead, nextDeadCell, hover, onClick }: Props ) => {
  const emptyCells = Array.from(Array(width))

  return (
    <div className="flex justify-center mb-1">
      {emptyCells.map((_, i) => (
        <Cell 
            key={i} 
            column={i}
            status={extractCellStatus(row, i, selectedPiece, owned, dead, nextDeadCell)}
            hover={(column: number) => hover(row, column)} 
            onClick={(column: number) => onClick(row, column)} 
        />
      ))}
    </div>
  )
}
