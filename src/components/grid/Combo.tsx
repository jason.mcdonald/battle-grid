import { Location } from "../../lib/pieces"
import { Cell } from './Cell'

type Props = {
  block: Location[]
}

const BLUE_DEFAULT = { status: 'Owned', color: 'blue', hoverStatus: 'None' }
const ORANGE_DEFAULT = { status: 'Owned', color: 'orange', hoverStatus: 'None' }

export const Combo = ({ block }: Props) => {

  return (
    <div className="flex justify-center">
        <div className="mx-4 scale-75">
            <div className="flex justify-center mb-1 items-center mx-0.5 text-3xl font-bold rounded dark:text-white">
                {"Combo"}
            </div>
            <div className="flex justify-center mb-1">
                <Cell column={0} key={1} status={BLUE_DEFAULT} hover={() => null} onClick={() => null} />
                <Cell column={1} key={2} status={BLUE_DEFAULT} hover={() => null} onClick={() => null} />
            </div>
            <div className="flex justify-center mb-1">            
                <Cell column={0} key={3} status={BLUE_DEFAULT} hover={() => null} onClick={() => null} />
                <Cell column={1} key={4} status={BLUE_DEFAULT} hover={() => null} onClick={() => null} />
            </div>
        </div>
        <div className="mx-4 scale-75">
            <div className="flex justify-center mb-1 items-center mx-0.5 text-3xl font-bold rounded dark:text-white">
                {"Combo"}
            </div>
            <div className="flex justify-center mb-1">
                <Cell column={0} key={1} status={BLUE_DEFAULT} hover={() => null} onClick={() => null} />
                <Cell column={1} key={2} status={ORANGE_DEFAULT} hover={() => null} onClick={() => null} />
            </div>
            <div className="flex justify-center mb-1">            
                <Cell column={0} key={3} status={BLUE_DEFAULT} hover={() => null} onClick={() => null} />
                <Cell column={1} key={4} status={ORANGE_DEFAULT} hover={() => null} onClick={() => null} />
            </div>
        </div>
    </div>
  )
}
