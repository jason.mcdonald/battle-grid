import { Location, Piece } from './pieces'

export const calculatePoints = (completedBlocks: any[]) => {
    let pointSum = 0

    for (let block of completedBlocks) {
        pointSum += allOneColor(block) ? 8 : 4
    }

    return pointSum * completedBlocks.length
}

const allOneColor = (block: Location[]) : boolean => {
    let colorSet = new Set(block.map((location) => location.color))

    return colorSet.size === 1
}

export const removeCompletedBlocks = (current: Location[], completedBlocks: any[], deadCells: Location[]) => {
    let newCurrent = [ ...current ]

    for (let block of completedBlocks) {
        for (let location of block) {
            newCurrent = newCurrent.filter((l) => !(l.x === location.x && l.y === location.y))
        }
    }

    for (let location of deadCells) {
        newCurrent = newCurrent.filter((l) => !(l.x === location.x && l.y === location.y))
    }

    return newCurrent
}

export const extractLocations = (piece: Piece) => {
    let locations: Location[] = []

    for (let point of piece.dimensions) {
        locations.push({ x: piece.origin.x + point.x, y: piece.origin.y + point.y, color: piece.color })
    }

    return locations
}