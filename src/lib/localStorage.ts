const gameStateKey = 'gameState'
const unlockUsesKey = 'unlockUses'

type StoredGameState = {
  guesses: string[]
  solution: string
}

type UnlockUsesState = {
  uses: number[]
}

export const saveGameStateToLocalStorage = (gameState: StoredGameState) => {
  localStorage.setItem(gameStateKey, JSON.stringify(gameState))
}

export const loadGameStateFromLocalStorage = () => {
  const state = localStorage.getItem(gameStateKey)
  return state ? (JSON.parse(state) as StoredGameState) : null
}

export const loadUnlockUsesFromLocalStorage = () => {
  const uses = localStorage.getItem(unlockUsesKey)
  return uses ? (JSON.parse(uses) as UnlockUsesState) : {uses: []}
}

export const saveUnlockUsesToLocalStorage = (unlockState: UnlockUsesState) => {
  localStorage.setItem(unlockUsesKey, JSON.stringify(unlockState))
}

const gameStatKey = 'gameStats'

export type GameStats = {
  winDistribution: number[]
  gamesFailed: number
  currentStreak: number
  bestStreak: number
  totalGames: number
  successRate: number
}

export const saveStatsToLocalStorage = (gameStats: GameStats) => {
  localStorage.setItem(gameStatKey, JSON.stringify(gameStats))
}

export const loadStatsFromLocalStorage = () => {
  const stats = localStorage.getItem(gameStatKey)
  return stats ? (JSON.parse(stats) as GameStats) : null
}
