# Block Challenge


## Daily Challenge

Each day is an unique challenge. Block and Dead cell order are tied to a daily seed to ensure that the challenge is consistent across all players.

## TODO

- [ ] Combo Blocks - Bonus points for completing combo blocks. these should be prebuilt by seed.
- [ ] Hold Function
- [ ] Drag and Drop Mobile Alternative


## Roguelike Block Challege

Building on the system defined in the daily challenge, add a simple rpg battle system on top. Points recieved from completed blocks translate to attack power.
Combo blocks are changed to defensive moves like Parry. Completing a combo block results in the next strike recieved from an opponent to be deflected, etc.
Opponents will simply perform a action on a interval. Restoring health is done outside of battle. Speed in attacking and Skill in completing combo blocks will be prioritized for managing health. Once a grid is unplayable a reset option will be available for -X Health.

Leveling up will be simple and use a grid to power it. Base Attack will be determined by the height of the stack for a max of 10. Levels will be gained for each
completed row similar to tetris. Once a row is completed then a level is gained and that row is wiped from the board just like tetris. Each experience point grants a new block to place on the level up board. Unique scenarios can be granted from this balance, go all in on attack results in a glass cannon. Focus on levels will mean it's hard to grow your attack. Experience can also be used to wipe out a row but without gaining a level. This would be a undesirable option only necessary if the board needed to be cleaned up to properly progress.


![LevelUp](./readme/LevelUp.png)

Roguelite Exploring will be a system inspired by FTL. You move a across a grid, left to right. Each piece placed results in a battle and represents your current position. Over time a enemy wave chases you across the grid. Events will be litered across the grid making you weigh the benefits of risking travel to them. Each explore grid can be thougth of as a scenario or level. Perhaps ending with a unique encounter.

![Explore](./readme/Explore.png)

These three screens will represent the game. Explore, Battle and Level up. Shallow and simple in concept but accessible with a consistent and unique asthetic. 

## TODO 

- [ ] Level Up Screen
    - [x] Grid & Placement Logic
    - [x] Level up and Attack calculation
    - [ ] Integrate into Game
- [ ] Explore System
    - [x] Grid & Placement Logic 
    - [x] Dying Cell Chase System
    - [x] Ability to place events
    - [ ] Place Destination node
    - [ ] Generate Randomized Events
    - [ ] Block Placement Triggers Battle or Event
- [ ] Battle System
    - [ ] Grid & Placement Logic
    - [ ] Combo/Parry System
    - [ ] Player Representation
    - [ ] Enemy Representation
    - [ ] Battle Flow and Logic
- [ ] Dialog System for conveying information